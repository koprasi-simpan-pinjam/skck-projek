/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.Model;

import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author acer
 */
public class TableModelData extends AbstractTableModel{
    List<ModelDataPribadi> listData;
    
    public TableModelData(List<ModelDataPribadi> listData) {
        this.listData = listData;
    }

    @Override
    public int getRowCount() {
        return listData.size();
    }

    @Override
    public int getColumnCount() {
        return 16;
    }

    @Override
    public Object getValueAt(int row, int col) {
        switch(col) {
            case 0:
                return listData.get(row).getIdDataPribadi();
            case 1:
                return listData.get(row).getNama();
            case 2:
                return listData.get(row).getTempatLahir();
            case 3:
                return listData.get(row).getTanggalLahir();
            case 4:
                return listData.get(row).getTempatLahir();
            case 2:
                return listData.get(row).getTempatLahir();
            case 2:
                return listData.get(row).getTempatLahir();
            case 2:
                return listData.get(row).getTempatLahir();
            case 2:
                return listData.get(row).getTempatLahir();
            case 2:
                return listData.get(row).getTempatLahir();
            case 2:
                return listData.get(row).getTempatLahir();
            case 2:
                return listData.get(row).getTempatLahir();
            case 2:
                return listData.get(row).getTempatLahir();
            case 2:
                return listData.get(row).getTempatLahir();
            default:
                return null;
        } 
    }
}
