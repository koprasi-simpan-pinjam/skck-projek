/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.Dao;

import java.util.List;
import skck.Model.ModelDataPribadi;

/**
 *
 * @author acer
 */
public interface ImplementDataPribadi {
    public void insert(ModelDataPribadi b);
    
    public void update(ModelDataPribadi b);
    
    public void delete(int id);
    
    public List<ModelDataPribadi> getAll();
}
