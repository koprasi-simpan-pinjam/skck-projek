/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import skck.Koneksi.koneksi;
import skck.Model.ModelDataPribadi;

/**
 *
 * @author acer
 */
public class DaoDataPribadi implements ImplementDataPribadi{
    Connection con;
    final String insert = "INSERT INTO # (field1, field2, field3, field4) VALUES (?, ?, ?, ?);";
    final String update = "UPDATE # SET field1=?, field2=?, field3=?, field4=? where id_field=?;";
    final String delete = "DELETE FROM barang where id_barang=?;";
    final String select = "SELECT * FROM #;";
    final String carinama = "SELECT * FROM # where fieldpencarian like ?;";
    
    public DaoDataPribadi() {
        con = koneksi.connection();
    }

    public void insert(ModelDataPribadi b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(insert);
            st.setString(1, b.getNama());
            st.setString(2, b.getTempatLahir());
            st.setDate(3, b.getTanggalLahir());
            st.setString(4, b.getGender());
            st.setString(5, b.getStatusPerkawinan());
            st.setString(6, b.getKewarganegaraan());
            st.setString(7, b.getAgama());
            st.setString(8, b.getPekerjaan());
            st.setString(9, b.getNoTelpon());
            st.setString(10, b.getAlamat());
            st.setString(11, b.getProvinsi());
            st.setString(12, b.getKabupaten());
            st.setString(13, b.getKecamatan());
            st.setString(14, b.getKelurahan());
            st.setString(15, b.getNoIdentitas());
            st.setString(16, b.getNoPaspor());
            
            st.executeUpdate();
//            ResultSet rs = st.getGeneratedKeys();
//            while(rs.next()) {
//                b.setIdPenerbit(rs.getInt(1));
//            }
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void update(ModelDataPribadi b) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(update);
            st.setString(1, b.getNama());
            st.setString(2, b.getTempatLahir());
            st.setDate(3, b.getTanggalLahir());
            st.setString(4, b.getGender());
            st.setString(5, b.getStatusPerkawinan());
            st.setString(6, b.getKewarganegaraan());
            st.setString(7, b.getAgama());
            st.setString(8, b.getPekerjaan());
            st.setString(9, b.getNoTelpon());
            st.setString(10, b.getAlamat());
            st.setString(11, b.getProvinsi());
            st.setString(12, b.getKabupaten());
            st.setString(13, b.getKecamatan());
            st.setString(14, b.getKelurahan());
            st.setString(15, b.getNoIdentitas());
            st.setString(16, b.getNoPaspor());
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void delete(int id) {
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(delete);
            
            st.setInt(1, id);
            st.executeUpdate();
            
        } catch(SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public List<ModelDataPribadi> getAll() {
        List<ModelDataPribadi> listDataPribadi = null;
        try {
            listDataPribadi = new ArrayList<>();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(select);
            while(rs.next()) {
                ModelDataPribadi b = new ModelDataPribadi();
                b.setIdDataPribadi(rs.getInt("id_barang"));
                b.setNama(rs.getString("namaBarang"));
                b.setTempatLahir(rs.getString("jumlahBarang"));
                b.setTanggalLahir(rs.getDate("tanggalBeli"));
                b.setGender(rs.getString("hargaBarang"));
                b.setStatusPerkawinan(rs.getString("hargaBarang"));
                b.setKewarganegaraan(rs.getString("hargaBarang"));
                b.setAgama(rs.getString("hargaBarang"));
                b.setPekerjaan(rs.getString("hargaBarang"));
                b.setNoTelpon(rs.getString("hargaBarang"));
                b.setAlamat(rs.getString("hargaBarang"));
                b.setProvinsi(rs.getString("hargaBarang"));
                b.setKabupaten(rs.getString("hargaBarang"));
                b.setKecamatan(rs.getString("hargaBarang"));
                b.setKelurahan(rs.getString("hargaBarang"));
                b.setNoIdentitas(rs.getString("hargaBarang"));
                b.setNoPaspor(rs.getString("hargaBarang"));
                listDataPribadi.add(b);
            }
        } catch(SQLException ex) {
            Logger.getLogger(DaoDataPribadi.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listDataPribadi;
    }
}
