/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.Controller;

import java.util.List;
import skck.Dao.DaoDataPribadi;
import skck.Dao.ImplementDataPribadi;
import skck.Model.TableModelData;
import skck.View.Frame;

/**
 *
 * @author acer
 */
public class ControllerDataPribadi {
    Frame fDataPribadi;
    ImplementDataPribadi iDataPribadi;
    List<ModelDataPibadi> lDataPribadi;
    
    public ControllerDataPribadi(Frame fDataPribadi) {
        this.fDataPribadi = fDataPribadi;
        iDataPribadi = new DaoDataPribadi();
        lDataPribadi = iDataPribadi.getAll();
    }
    
//     reset field
    public void reset() {
        fDataPribadi.getIdDataPribadi().setText("");
        fDataPribadi.getNama().setText("");
        fDataPribadi.getKelasSiswa().setText("");
    }
    
    // menampilkan data ke dalam tabel
    public void isiTabel() {
        lDataPribadi = iDataPribadi.getAll();
        TableModelData tmDataPribadi = new TableModelData(lDataPribadi);
        fDataPribadi.getTabelData().setModel(tmDataPribadi);
    }
    
    // menampilkan data yang dipilih dari tabel
//    public void isiField(int row) {
//        fDataPribadi.getIdDataPribadi().setText(lDataPribadi.get(row).getIdDataPribadi().toString());
//        fDataPribadi.getNamaBarang().setText(lDataPribadi.get(row).getNamaBarang());
//        fDataPribadi.getJumlahBarang().setText(lDataPribadi.get(row).getJumlahBarang().toString());
//        fDataPribadi.getTanggalBeli().setText(lDataPribadi.get(row).getTanggalBeli().toString());
//        fDataPribadi.getHargaBarang().setText(lDataPribadi.get(row).getHargaBarang().toString());
//        
//    }
    
   // insert data 
    public void insert() {
        ModelDataPibadi b = new ModelDataPibadi();
        b.setNama_siswa(fSiswa.getNamaSiswa().getText()); 
        b.setKelas_siswa(fSiswa.getKelasSiswa().getText());
        
        iSiswa.insert(b);
    }
    
    //Update data
    public void update() {
        SiswaModel b = new SiswaModel();
        b.setNama_siswa(fSiswa.getNamaSiswa().getText());
        b.setKelas_siswa(fSiswa.getKelasSiswa().getText());
        b.setId_siswa(Integer.parseInt(fSiswa.getIDSiswa().getText()));
       
        iSiswa.update(b);
    }
    
    //delete data 
    public void delete() {
        if(!fBarang.getIDBarang().getText().trim().isEmpty()) {
            int id = Integer.parseInt(fBarang.getIDBarang().getText());
            iBarang.delete(id);
        } else {
            JOptionPane.showMessageDialog(fBarang, "Silahkan Hapus");
        }
    }
}
